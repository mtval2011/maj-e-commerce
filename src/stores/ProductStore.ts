import {defineStore} from 'pinia';
import productsJson from '@/assets/products.json';
import {ref, computed} from 'vue';
import type ProductModel from '@/components/models/ProductModel';
import ProductDetailVue from '@/components/ProductDetail.vue';
import type CartModel from '@/components/models/CartModel';

export const useProductStore = defineStore('product', () => {
    
    const products = ref<ProductModel[]> (productsJson);
    const product = ref<ProductModel>();
    const search = ref('');
    const catalog = ref(products);
    const productDetail = ref<ProductModel>();
    const cartData = ref<CartModel[]>([]);
    const productsOfCarts = ref<ProductModel[]>([]);

    const filterList = computed(() =>
    catalog.value?catalog.value.filter(product =>
        product.title?.toLocaleLowerCase()
        .includes(search.value)):products.value);
    
    const canBuy = (id: Number) => {
        console.log('Can buy product Id ------->>>>>>', id);
        product.value = products.value.find(p => p.id ===id);
        if(product.value){
            productsOfCarts.value.push(product.value);

            cartData.value = [];
            if(productsOfCarts.value)
                mapProductsToCarts(productsOfCarts.value);
        }
    }

    const canViewDetail = (id: Number) => {
        console.log('Can view detail of product Id ------->>>>>>', id);
        productDetail.value = products.value.find(p => p.id ===id);
    }

    const mapProductsToCarts = (products: Array<ProductModel>) =>{
        if(products !== undefined) {
            const carts = products.flatMap(p => mapper(p, products));
            const ids =carts.map(c => c.id);
            cartData.value = carts
            .filter(({id}, index) => !ids.includes(id, index+1));
        }
    }
    const mapper = (product: ProductModel,
        list: Array<ProductModel>): CartModel =>{        
        const cart: CartModel = {id:0, quantity:0}
        cart.id = product.id;
        cart.title = product.title;
        cart.price = product.price;
        cart.quantity = list.filter(({id}) => id === product.id).length;
        return cart;
    }
    return {filterList, search, productDetail, cartData, canBuy, canViewDetail};
})