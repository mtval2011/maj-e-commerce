import { defineStore, storeToRefs } from "pinia";
import { useProductStore } from "./ProductStore";
import { computed } from "vue";

export const useCartStore = defineStore('cart', () =>{
    const {cartData} = storeToRefs(useProductStore());
    const total = computed(() => cartData.value
    .reduce((cumule, p) => cumule+=(p.quantity*p.price), 0));
    const totalElements = computed(() => cartData.value
    .reduce((sum, p) => sum+=p.quantity, 0));

    return {cartData, total, totalElements};
});