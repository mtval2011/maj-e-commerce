export default interface Rating{
    rate?: Number,
    count?: Number
}