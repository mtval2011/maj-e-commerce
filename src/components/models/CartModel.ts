export default interface CartModel{
    id: Number,
    title?: String,
    price?: Number,
    quantity?: Number
}