import type Rating from "./RatingModel";
export default interface Product{
    id: Number,
    title?: String,
    price?: Number,
    description?: String,
    categorie?: String,
    image?: String
    Rating?: Rating
}